```
$ docker compose up -d

$ make
redis-cli -p 6379 del test
(integer) 1
python3 ./import/bulk_insert.py test \
		-n ./data/addresses.csv \
		-r ./data/before_rel.csv \
		-r ./data/belongs_to_rel.csv \
		-n ./data/blocks.csv \
		-n ./data/transactions.csv
addresses  [####################################]  100%
10055 nodes created with label 'addresses'
blocks  [####################################]  100%
100001 nodes created with label 'blocks'
transactions  [####################################]  100%
216575 nodes created with label 'transactions'
before_rel  [####################################]  100%
100000 relations created for type 'before_rel'
belongs_to_rel  [####################################]  100%
216577 relations created for type 'belongs_to_rel'
Construction of graph 'test' complete: 326631 nodes created, 316577 relations created in 10.179595 seconds

$ make test
redis-cli -p 6379 graph.query test "MATCH(t:transactions)-[r:belongs_to_rel]->() RETURN t, r LIMIT 10"
1) 1) "t"
   2) "r"
2)  1) 1) 1) 1) "id"
             2) (integer) 110056
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b"
       2) 1) 1) "id"
             2) (integer) 100000
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110056
          4) 1) "dest_node"
             2) (integer) 10055
          5) 1) "properties"
             2) (empty array)
    2) 1) 1) 1) "id"
             2) (integer) 110057
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098"
       2) 1) 1) "id"
             2) (integer) 100001
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110057
          4) 1) "dest_node"
             2) (integer) 10056
          5) 1) "properties"
             2) (empty array)
    3) 1) 1) 1) "id"
             2) (integer) 110058
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "9b0fc92260312ce44e74ef369f5c66bbb85848f2eddd5a7a1cde251e54ccfdd5"
       2) 1) 1) "id"
             2) (integer) 100002
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110058
          4) 1) "dest_node"
             2) (integer) 10057
          5) 1) "properties"
             2) (empty array)
    4) 1) 1) 1) "id"
             2) (integer) 110059
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "999e1c837c76a1b7fbb7e57baf87b309960f5ffefbf2a9b95dd890602272f644"
       2) 1) 1) "id"
             2) (integer) 100003
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110059
          4) 1) "dest_node"
             2) (integer) 10058
          5) 1) "properties"
             2) (empty array)
    5) 1) 1) 1) "id"
             2) (integer) 110060
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "df2b060fa2e5e9c8ed5eaf6a45c13753ec8c63282b2688322eba40cd98ea067a"
       2) 1) 1) "id"
             2) (integer) 100004
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110060
          4) 1) "dest_node"
             2) (integer) 10059
          5) 1) "properties"
             2) (empty array)
    6) 1) 1) 1) "id"
             2) (integer) 110061
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "63522845d294ee9b0188ae5cac91bf389a0c3723f084ca1025e7d9cdfe481ce1"
       2) 1) 1) "id"
             2) (integer) 100005
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110061
          4) 1) "dest_node"
             2) (integer) 10060
          5) 1) "properties"
             2) (empty array)
    7) 1) 1) 1) "id"
             2) (integer) 110062
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "20251a76e64e920e58291a30d4b212939aae976baca40e70818ceaa596fb9d37"
       2) 1) 1) "id"
             2) (integer) 100006
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110062
          4) 1) "dest_node"
             2) (integer) 10061
          5) 1) "properties"
             2) (empty array)
    8) 1) 1) 1) "id"
             2) (integer) 110063
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "8aa673bc752f2851fd645d6a0a92917e967083007d9c1684f9423b100540673f"
       2) 1) 1) "id"
             2) (integer) 100007
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110063
          4) 1) "dest_node"
             2) (integer) 10062
          5) 1) "properties"
             2) (empty array)
    9) 1) 1) 1) "id"
             2) (integer) 110064
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "a6f7f1c0dad0f2eb6b13c4f33de664b1b0e9f22efad5994a6d5b6086d85e85e3"
       2) 1) 1) "id"
             2) (integer) 100008
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110064
          4) 1) "dest_node"
             2) (integer) 10063
          5) 1) "properties"
             2) (empty array)
   10) 1) 1) 1) "id"
             2) (integer) 110065
          2) 1) "labels"
             2) 1) "transactions"
          3) 1) "properties"
             2) 1) 1) "txid"
                   2) "0437cd7f8525ceed2324359c2d0ba26006d92d856a9c20fa0241106ee5a597c9"
       2) 1) 1) "id"
             2) (integer) 100009
          2) 1) "type"
             2) "belongs_to_rel"
          3) 1) "src_node"
             2) (integer) 110065
          4) 1) "dest_node"
             2) (integer) 10064
          5) 1) "properties"
             2) (empty array)
3) 1) "Cached execution: 0"
   2) "Query internal execution time: 64.098363 milliseconds"