all: clean import

.PHONY: import
import:
	python3 ./import/bulk_insert.py test \
		-n ./data/addresses.csv \
		-r ./data/before_rel.csv \
		-r ./data/belongs_to_rel.csv \
		-n ./data/blocks.csv \
		-n ./data/transactions.csv

.PHONY: test
test:
	redis-cli -p 6379 graph.query test "MATCH(t:transactions)-[r:belongs_to_rel]->() RETURN t, r LIMIT 10"

.PHONY: clean
clean:
	redis-cli -p 6379 del test